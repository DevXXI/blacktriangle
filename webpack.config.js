var path = require('path');
var webpack = require('webpack');


module.exports = {
  resolve: {
    extensions : ['', '.js', '.jsx']
  },
  entry : [
    'babel-polyfill',
    './src/theme/main.scss',
    './src/main.js',
    'webpack-dev-server/client?http://localhost:8080'
  ],
  output : {
    publicPath : '/',
    filename: 'bundle.js'
  },
  devtool : 'source-map',
  debug: true,
  module: {
    loaders : [
      {
        test: /\.js$/,
        include: path.join(__dirname, 'src'),
        loader: 'babel-loader',
        query: {
          presets : ['es2015']
        }
      },
      {
        test: /\.scss$/,
        loaders : ['style','css?sourceMap', 'sass?sourceMap']
      }
    ]
  }
};
